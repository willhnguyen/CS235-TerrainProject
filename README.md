# CS 245 - Motion Path Animation Editor

Author: William Nguyen
Date: Thursday, May 10, 2018

## Keyboard Toggles

| Keypress | Action |
|-|-|
| T | Camera Retarget (camera looks at the selected point or rover) |
| P | Point Selection Mode |
| M | Motion Path Selection Mode |
| D | Delete currently selected point if it is on the defined Motion Path |
| Space | Start animation (rover will follow the motion path) |
| R | Resets the Camera to the default view |
| Tab | Cycles through the camera modes |
| 0 | Default View (same as reset keypress 'R') |
| 1 | Rover Front Camera View |
| 2 | Tracking Camera View (aerial shot of rover) |
| 3 | Rover Follow View (camera is behind the rover and sees the entire rover) |
| 4 | Rover Rear Camera View |
| Cntl/Cmd + S | Save a motion path as a CSV file|
| Cntl/Cmd + O | Load a motion path |

## Issues
* Polyline mapping to terrain causes a jagged motion path. Can be avoided if motion path isn't mapped onto the terrain and just uses the user-selected points.
* Heading calculations causes randomly huge rotations when the rover approaches a user-defined point. Seems to be an issue with OpenFramework's ofPolyline class.
* Camera views could be adjusted to provide a better views.
* Rover only rotates to change its heading angle not its upward angle.
* To replay an animation, spacebar needs to be pressed twice. End-of-animation state isn't registered because there were original plans to provide a looped animation for which code exists.

## Comments
This project took a lot of time. Refactoring and rearranging code was done to make it easier for me to organize and understand the code. Creating classes like MotionPath required lengthy commitment to writing it without testing it until I was 90% finished with it. Playing with values for camera states took and testing potential heading and upward rotations took a long time and the latter resulted in no success.

Overall time to finish this project: two entire days.