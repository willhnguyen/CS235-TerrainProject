//
//  Octree.cpp
//  CS235-TerrainProject
//
//  Created by William Nguyen on 4/20/18.
//

#include "Octree.hpp"
#include "Helpers.hpp"
#include <queue>
#include <limits>

#define EPSILON 1e-10

// Initialize an OctreeNode with null default values
OctreeNode::OctreeNode() {
    // New nodes have no values or children with values so the count is
    // zero and the index is set to -1 which is out of bounds
    count = 0;
    index = -1;
    
    // Set all 8 children to NULL
    for (auto& child : children) { child = NULL; }
}
OctreeNode::~OctreeNode() {
    // Free up the pointer memory
    for (auto& child : children) {
        if (child) {
            delete child;
            child = NULL;
        }
    }
}

// Create an octree from the mesh
Octree::Octree(const ofMesh &mesh) {
    // Get vertex list used to process and create tree
    vertexList = mesh.getVertices();
    count = vertexList.size();
    
    // If the vertex list is empty, then there's nothing to do
    if (!count) { return; }
    
    // Create the root node to add a value
    root = new OctreeNode();
    root->boundingBox = getBoundingBox();
    
    // Build the tree by sending lists of vertices
    int indexList[count];
    for (int i = 0; i < count; ++i) {
        indexList[i] = i;
    }
    buildOctree(root, indexList, count);
}
Octree::~Octree() {
    // Free and reset pointer memory
    if (root) {
        delete root;
        root = NULL;
    }
}

// Build the octree at the specified node with the specified index list
// The index list is split among children nodes until it's size 1 in which case its sole value is given to a leaf node
void Octree::buildOctree(OctreeNode *node, int *indexList, int count) {
    node->count = count;

    // If the indexList is size 1, then the node is a leaf node
    if (count == 1) {
        node->index = indexList[0];
        return;
    }

    // Otherwise, determine box midpoints to create children nodes
    Vector3 minBound = node->boundingBox.min();
    Vector3 maxBound = node->boundingBox.max();
    double midBoundx = (minBound.x() + maxBound.x()) / 2,
    midBoundy = (minBound.y() + maxBound.y()) / 2,
    midBoundz = (minBound.z() + maxBound.z()) / 2;

    // Split indices for each child node
    int *childrenIndexLists[8];
    for (auto& childIndexList : childrenIndexLists) {
        childIndexList = new int[count];
    }
    int childrenIndexListCounts[8] = {0, 0, 0, 0, 0, 0, 0, 0};

    // Determine which indices belong to which child
    for (int i = 0; i < count; ++i) {
        int index = indexList[i];
        // Determine which list the vertex belongs to and add index to child's index list
        const ofVec3f &vertex = vertexList[index];
        int childIndex =
            (vertex.x > midBoundx ? 1 : 0) +
            (vertex.y > midBoundy ? 2 : 0) +
            (vertex.z > midBoundz ? 4 : 0);

        childrenIndexLists[childIndex][childrenIndexListCounts[childIndex]] = index;
        ++childrenIndexListCounts[childIndex];
    }

    // Create children nodes
    for (int i = 0; i < 8; ++i) {
        bool isRight = i & 1;
        bool isTop   = i & 2;
        bool isFront = i & 4;

        // If child has no vertex, then don't create node
        if (!childrenIndexListCounts[i]) { continue; }

        // If child has at least one vertex, then create a node and recurse
        OctreeNode *newChild = new OctreeNode;
        newChild->boundingBox = Box(
                                    Vector3(
                                            (isRight ? midBoundx : minBound.x()),
                                            (isTop   ? midBoundy : minBound.y()),
                                            (isFront ? midBoundz : minBound.z())
                                            ),
                                    Vector3(
                                            (isRight ? maxBound.x() : midBoundx),
                                            (isTop   ? maxBound.y() : midBoundy),
                                            (isFront ? maxBound.z() : midBoundz)
                                            )
                                    );
        node->children[i] = newChild;
        buildOctree(newChild, childrenIndexLists[i], childrenIndexListCounts[i]);

        // Free memory
        delete childrenIndexLists[i];
    }
}

// Get a box that bounds all the vertices defined in the vertex list
// Epsilon is added to give the box some breathing space to ensure it captures edge cases where vertices
// might lie on the edge of the box
Box Octree::getBoundingBox() {
    // If the vertexlist is empty, then return a box of size zero
    if (vertexList.empty()) { return Box(); }
    
    // Determine the min and max bounds
    float minBoundx = FLT_MAX;
    float minBoundy = FLT_MAX;
    float minBoundz = FLT_MAX;
    
    float maxBoundx = FLT_MIN;
    float maxBoundy = FLT_MIN;
    float maxBoundz = FLT_MIN;
    
    for (auto vertex : vertexList) {
        float vertexx = vertex.x;
        float vertexy = vertex.y;
        float vertexz = vertex.z;
        
        minBoundx = min(minBoundx, vertexx);
        minBoundy = min(minBoundy, vertexy);
        minBoundz = min(minBoundz, vertexz);
        
        maxBoundx = max(maxBoundx, vertexx);
        maxBoundy = max(maxBoundy, vertexy);
        maxBoundz = max(maxBoundz, vertexz);
    }
    
    // Extend bounds for edge cases
    minBoundx -= EPSILON;
    minBoundy -= EPSILON;
    minBoundz -= EPSILON;
    
    maxBoundx += EPSILON;
    maxBoundy += EPSILON;
    maxBoundz += EPSILON;
    
    // Return box with the new min and max bounds
    return Box(Vector3(minBoundx, minBoundy, minBoundz), Vector3(maxBoundx, maxBoundy, maxBoundz));
}

// Select the closest vertex to the provided ray origin
// Use a priority queue/heap to ensure searching closest boxes/nodes first
int Octree::selectWithRay(const Ray &ray) {
    // If the root node doesn't exist, then there is nothing to seelct
    if (!root) {
        return -1;
    }
    
    // First check if the ray intersects the bounding box of the root node
    // If not, then there's nothing to search
    if (!(root->boundingBox.intersect(ray, -100, 100))) {
        return -1;
    }
    
    // If there is an intersection, then perform the search
    // Create priority queue
    typedef priority_queue<PriorityQueueNode, vector<PriorityQueueNode>, PriorityQueueNode_Comparator> OctreeNodeHeap;
    OctreeNodeHeap minDistNodesHeap;
    
    // Add root node as first element of priority queue
    PriorityQueueNode heapNode;
    // Create heapNode to push the node information onto the heap
    heapNode.node = root;
    heapNode.dist = distanceBetween(ray.origin, root->boundingBox);
    minDistNodesHeap.push(heapNode);
    
    double closest_box = DBL_MAX;
    int closest_index = -1;
    
    // Perform a nearest-first search
    // Similar to breadth-first search but instead of checking the earliest seen node, check the closest node
    while (!minDistNodesHeap.empty()) {
        // Pop from heap
        heapNode = minDistNodesHeap.top();
        minDistNodesHeap.pop();
        int heapNodeCount = heapNode.node->count;
        
        // If the current node is a leaf node (has one value only),
        // then the desired vertex is found and can be returned
        if (heapNodeCount == 1) {
            // Recalculate distance
            heapNode.dist = distanceBetween(ray, vertexList[heapNode.node->index]);
            
            if (heapNode.dist < closest_box) {
                closest_box = heapNode.dist;
                closest_index = heapNode.node->index;
            }
        }
        
        // Otherwise, push (non-NULL) children which the ray intersects onto the heap
        else if (heapNodeCount > 1) {
            for (auto child : heapNode.node->children) {
                if (child != NULL && child->boundingBox.intersect(ray, -100, 100)) {
                    // Update heapNode to push the correct child node information onto the heap
                    heapNode.node = child;
                    heapNode.dist = distanceBetween(ray.origin, child->boundingBox);
                    minDistNodesHeap.push(heapNode);
                }
            }
        }
    }
    
    // If no nodes are found, then return -1 as an invalid index
    return closest_index;
}

int Octree::selectWithRayNaive(const Ray &ray) {
    return _selectWithRayNaive(root, ray);
}
int Octree::_selectWithRayNaive(OctreeNode *node, const Ray &ray) {
    if (node == NULL) { return -1; }
    
    // If node is leaf node, then return index
    if (node->count == 1) {
        return node->index;
    }
    
    // If node is internal node, then cycle through children and choose closest vertex
    int closest_index = -1;
    double closest_distance = DBL_MAX;
    for (auto child : node->children) {
        // Ingore child if ray doesn't intersect the child's bounding box
        if (child == NULL || !(child->boundingBox.intersect(ray, -100, 100))) {
            continue;
        }
        
        // Get index and continue if index is -1 (no point selected)
        int index = _selectWithRayNaive(child, ray);
        if (index == -1) { continue; }
        
        // Determine if child is closer
        double distance = distanceBetween(ray, vertexList[index]);
        if (distance < closest_distance) {
            closest_index = index;
            closest_distance = distance;
        }
    }
    
    return closest_index;
}

/* Returns the vertex at the defined index.
 *
 * @param {int} index The index of the desired vertex in vertexList.
 * @return {ofVec3f} The desired vertex at the defined index in vertexList.
 */
ofVec3f Octree::getVertexAtIndex(int index) {
    // Bounds checking
    if (index < 0 || index >= vertexList.size()) {
        cout << "ERROR: Cannot access a vertex at index " << index << " since it is outside the bounds of the vector of size " << vertexList.size() << endl;
        
        return ofVec3f(-100, -100, -100); // Return an arbitrary point
    }
    
    // Return desired vertex
    return vertexList[index];
}

// Draw a specified box
void Octree::drawBox(const Box &box) {
    // Copied from the provided sample code
    Vector3 min = box.parameters[0];
    Vector3 max = box.parameters[1];
    Vector3 size = max - min;
    Vector3 center = size / 2 + min;
    ofVec3f p = ofVec3f(center.x(), center.y(), center.z());
    float w = size.x();
    float h = size.y();
    float d = size.z();
    ofDrawBox(p, w, h, d);
}

// Draw bounding boxes of each node up until the specified level
void Octree::drawBoxesUntilLevel(int level) {
    drawBoxesUntilLevelFromNode(level, root);
}
void Octree::drawBoxesUntilLevelFromNode(int level, OctreeNode *node) {
    // Cannot draw for an empty tree
    if (!node) {
        return;
    }
    // Don't draw if level reached already
    if (level <= 0) {
        return;
    }
    
    // Draw a box for the current node and recurse for all children nodes
    drawBox(node->boundingBox);
    for (auto child : node->children) {
        drawBoxesUntilLevelFromNode(level-1, child);
    }
}

// Draw bounding boxes of each node at the specified level or earlier if it's a leaf node
void Octree::drawBoxesAtLevel(int level) {
    drawBoxesAtLevelFromNode(level, root);
}
void Octree::drawBoxesAtLevelFromNode(int level, OctreeNode *node) {
    // Cannot draw for an empty tree
    if (!node) {
        return;
    }
    // Draw if level reached 0 or if it's a leaf node
    if (level <= 0 || node->count == 1) {
        drawBox(node->boundingBox);
        return;
    }
    
    // Recurse on all children
    for (auto child : node->children) {
        drawBoxesAtLevelFromNode(level-1, child);
    }
}

// Draw five levels of boxes
void Octree::drawFiveLevels() {
    drawFiveLevelsFromNode(0, root);
}
void Octree::drawFiveLevelsFromNode(int level, OctreeNode *node) {
    if (node == NULL) {
        return;
    }
    
    // Set the color and dra the box if the level is less than 5
    if (level < 5) {
        // Set color
        switch(level) {
            case 0:
                ofSetColor(ofColor::white);
                break;
            case 1:
                ofSetColor(ofColor::blue);
                break;
            case 2:
                ofSetColor(ofColor::green);
                break;
            case 3:
                ofSetColor(ofColor::yellow);
                break;
            case 4:
                ofSetColor(ofColor::red);
                break;
            default:
                ofSetColor(ofColor::darkGreen);
                break;
        }
        
        // Draw box
        drawBox(node->boundingBox);
    }
    
    // Recurse and draw boxes for children if they're less than 5 levels deep in the Octree
    if (level < 4) {
        for (auto child : node->children) {
            if (child) {
                drawFiveLevelsFromNode(level + 1, child);
            }
        }
    }
}


// Get the number of nodes created for the Octree
// For any tree, the max number of tree nodes is bounded by numVertices * 2
// For any n-tree, the most balanced tree is bounded by ceil((n * numVertices - 1) / (n-1))
int Octree::countNodes() {
    return _countNodes(root);
}
int Octree::_countNodes(OctreeNode *node) {
    if (!node) {
        return 0;
    }
    
    // Get the sum of children node counts
    int count = 0;
    for (auto child : node->children) {
        if (child) {
            count += _countNodes(child);
        }
    }
    
    // Return count from children nodes and count from self (the current node)
    return count + 1;
}

// Get the highest height/depth of the tree
// Helps analyze if the tree is built efficiently (i.e. shallow and not very deep)
// For octrees, the min tree height is bounded by ceil(log(numVertices) / log(8))
int Octree::getHeight() {
    return _getHeight(root);
}
int Octree::_getHeight(OctreeNode *node) {
    if (!node) {
        return 0;
    }
    
    // Get the max child height
    int count = -1;
    for (auto child : node->children) {
        if (child) {
            count = max(count, _getHeight(child));
        }
    }
    
    // Return height count from children nodes and count from self (the current node)
    return count + 1;
}

// Verify that the tree has been created correctly
// Each node has a count representing the number of vertices is in its bounding box
// If correctly created, then sum of children counts should equal the node's count
bool Octree::verifyTreeByCounts() {
    return _verifyTreeByCounts(root) != -1;
}
int Octree::_verifyTreeByCounts(OctreeNode *node) {
    if (!node) {
        return 0;
    }
    // Base case, if the node is a leaf node, then its count is already verified by its count
    if (node->count == 1) {
        return 1;
    }
    
    // If the tree is made correctly, then the node's count should equal the sum of children counts
    int count = 0;
    for (auto child : node->children) {
        if (child) {
            int childCount = _verifyTreeByCounts(child);
            
            // If the verification failed early on, then return -1
            if (childCount == -1) {
                return -1;
            }
            
            count += childCount;
        }
    }
    
    // Check that the summed counts equal the node's count
    // If not, then return -1
    if (count != node->count) {
        return -1;
    }
    
    return count;
}
