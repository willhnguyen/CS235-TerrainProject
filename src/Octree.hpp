//
//  Octree.hpp
//  CS235-TerrainProject
//
//  Created by William Nguyen on 4/20/18.
//

#ifndef Octree_hpp
#define Octree_hpp

#include <stdio.h>
#include <vector>
#include "ofMain.h"
#include "vector3.h"
#include "box.h"
#include "ray.h"

struct OctreeNode {
    int index;
    int count;
    Box boundingBox;
    OctreeNode * children[8];
    
    OctreeNode();
    ~OctreeNode();
};

class Octree {
public:
    Octree(const ofMesh &);
    ~Octree();
    
    // Methods for selection
    int selectWithRay(const Ray &);
    int selectWithRayNaive(const Ray &);
    ofVec3f getVertexAtIndex(int);
    
    // Methods for drawing bounding boxes
    void drawBoxesUntilLevel(int);
    void drawBoxesAtLevel(int);
    void drawFiveLevels();
    
    // Methods for verifying and analyzing tree
    int countNodes();
    int getHeight();
    bool verifyTreeByCounts();
private:
    int count;
    OctreeNode *root;
    vector<ofVec3f> vertexList;
    
    // Methods involved in building tree
    void buildOctree(OctreeNode *, int *, int);
    Box getBoundingBox();
    
    // Methods for selecting
    int _selectWithRayNaive(OctreeNode *, const Ray &);
    
    // Methods for drawing
    void drawBox(const Box &);
    void drawBoxesUntilLevelFromNode(int, OctreeNode *);
    void drawBoxesAtLevelFromNode(int, OctreeNode *);
    void drawFiveLevelsFromNode(int, OctreeNode *);
    
    // Methods for verifying and analyzing tree
    int _countNodes(OctreeNode *);
    int _getHeight(OctreeNode *);
    int _verifyTreeByCounts(OctreeNode *);
};


/**
 * Custom Priority Queue Elements
 * * Priority Queue Node
 * * Priority Queue Node Comparator
 */
struct PriorityQueueNode {
    OctreeNode *node;
    float dist;
};

// Comparator used for the priority queue/heap
// Copied from http://www.cplusplus.com/reference/queue/priority_queue/priority_queue/
// Edited for use with PriorityQueueNode
class PriorityQueueNode_Comparator {
    bool reverse;
public:
    PriorityQueueNode_Comparator(const bool& revparam=false) {
        reverse=revparam;
        
    }
    bool operator() (const PriorityQueueNode &lhs, const PriorityQueueNode &rhs) const {
        if (reverse) {
            return (lhs.dist > rhs.dist);
        }
        else {
            return (lhs.dist < rhs.dist);
        }
    }
};

#endif /* Octree_hpp */
