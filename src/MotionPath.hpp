//
//  MotionPath.hpp
//  CS235-TerrainProject
//
//  Created by William Nguyen on 5/9/18.
//

#ifndef MotionPath_hpp
#define MotionPath_hpp

#include <stdio.h>
#include <vector>
#include "ofMain.h"
#include "Octree.hpp"

class MotionPath {
public:
    MotionPath();
    ~MotionPath();
    
    // Motion path draw
    void draw(); // draws both path and points
    void drawPath();
    void drawPoints();

    // Motion path update methods
    void useOctree(Octree *);
    void createPathFromList(const vector<ofVec3f> &);
    void addPoint(const ofVec3f &);
    bool editPoint(int, const ofVec3f &);
    void clear();
    void deletePointAtIndex(int);
    
    // Motion path selection methods
    int selectWithRay(const Ray &);
    ofVec3f &getPoint(int);
    void hideIndex(int);
    void unhide();
    
    // Motion path animation property update methods
    void setSpeed(double);
    void setTraversalMode(int);
    void setNoLooping();
    void setForwardLooping();
    void setForwardBackwardLooping();
    
    // State update methods
    void moveByDeltaTime(double);
    void resetState();
    
    // Get currently defined path
    const vector<ofVec3f> &getPoints();
    
    // Get current position vectors
    vector<ofVec3f> getPosLookatUp(); // Returns (position, look at, up) vectors
    void getPosLookatUp(vector<ofVec3f> &); // Returns (position, look at, up) vectors
    ofVec3f getPosition();
    ofVec3f getLookAt();
    ofVec3f getLookAtDir();
    ofVec3f getUp();
    double getTheta();
    double getPhi();
    ofVec3f getRightVector();
    ofVec3f getRotationAxis();
    double getRotationAngle();

private:
    // Motion Path definition
    vector<ofVec3f> points;
    bool pathIsSet;
    ofPolyline path;
    Octree *tree;
    
    // Motion Path methods
    void setPath();
    ofPolyline generateCurve(const vector<ofVec3f> &);
    void generateCurve(const vector<ofVec3f> &, ofPolyline &);
    void mapPointsToTerrain(const vector<ofVec3f> &, vector<ofVec3f> &);
    
    // Display properties
    int hiddenPoint;
    
    // Traversal properties
    double targetSpeed;
    double totalPathLength;
    int traversalMode; // Modes: {0: no looping, 1: loop forwards continuously, 2: loop forwards and backwards continuously}
    
    // Current traversal state
    double speed;
    bool restartTraversal;
    double traversedLength;
    bool moveBackward;

};

#endif /* MotionPath_hpp */
