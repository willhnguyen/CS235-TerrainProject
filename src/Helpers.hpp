//
//  Helpers.hpp
//  CS235-TerrainProject
//
//  This file contains helper functions that are used by the main app. Some
//  functions from the starter code have been migrated here to organize the code
//  for readability.
//
//  Created by William Nguyen on 5/9/18.
//

#ifndef Helpers_hpp
#define Helpers_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "box.h"
#include "ray.h"
#include "vector3.h"

// Box methods
void drawBox(const Box &);
Box meshBounds(const ofMesh &);
Box meshBounds(ofxAssimpModelLoader &, double scale);
void subDivideBox8(const Box &, vector<Box> &);

// Draw methods
void drawAxis(ofVec3f);
void drawMesh(ofxAssimpModelLoader &, int);

// Interpolation methods
ofVec3f interpolate(double, ofVec3f &, ofVec3f &);

// Object Creation methods
Ray createRay(ofVec3f &, ofVec3f &);

// Conversion Methods
Vector3 convertofVec2Vec(ofVec3f &);
ofVec3f convertVec2ofVec(Vector3 &);

// Ray intersect Methods
bool rayIntersectPlane(const ofVec3f &rayPoint, const ofVec3f &raydir, ofVec3f const &planePoint, const ofVec3f &planeNorm, ofVec3f &point);
bool rayIntersectPlane(const Ray &ray, ofVec3f const &planePoint, const ofVec3f &planeNorm, ofVec3f &point);

// Miscellaneous
ofVec3f reflectVector(const ofVec3f &v, const ofVec3f &normal);

// Get the box mid values
Vector3 getBoxMid(Box &);

// Distance Methods
double distanceBetween(Box &a, Box &b);
double distanceBetween(Box &a, const ofVec3f &b);
double distanceBetween(Box &a, const Ray &b);
double distanceBetween(Box &a, const Vector3 &b);

double distanceBetween(const Vector3 &a, Box &b);
double distanceBetween(const Vector3 &a, const ofVec3f &b);
double distanceBetween(const Vector3 &a, const Ray &b);
double distanceBetween(const Vector3 &a, const Vector3 &b);

double distanceBetween(const ofVec3f &a, Box &b);
double distanceBetween(const ofVec3f &a, const ofVec3f &b);
double distanceBetween(const ofVec3f &a, const Ray &b);
double distanceBetween(const ofVec3f &a, const Vector3 &b);

double distanceBetween(const Ray &a, Box &b);
double distanceBetween(const Ray &a, const ofVec3f &b);
//double distanceBetween(const Ray &a, const Ray &b);
double distanceBetween(const Ray &a, const Vector3 &b);

#endif /* Helpers_hpp */
