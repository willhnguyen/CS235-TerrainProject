//
//  Model.cpp
//  CS235-TerrainProject
//
//  Created by William Nguyen on 5/9/18.
//

#include "Model.hpp"
#include "Helpers.hpp"

Model::Model() {
}
Model::~Model() {
}

/* Set the model's actual model.
 *
 * @param {ofxAssimpModelLoader &} model The model to copy into the class's _model object.
 */
void Model::setModel(ofxAssimpModelLoader &model, double scale) {
    _model = model;
    
    // Scale the model
    _model.setScale(scale, scale, scale);
    
    // Calculate the bounding box
    Box box = meshBounds(_model, scale);
    _box = box;
    
    // Get the bounding box's min and max values
    Vector3 box_min = box.min();
    Vector3 box_max = box.max();
    
    _box_min = ofVec3f(box_min.x(), box_min.y(), box_min.z());
    _box_max = ofVec3f(box_max.x(), box_max.y(), box_max.z());
}

/* Set the position of the model.
 *
 * @param {ofVec3f &} position The new position of the model.
 */
void Model::setPosition(ofVec3f &position) {
    _model.setPosition(position.x, position.y, position.z);
    
    // Determine the box after model repositioning
    ofVec3f new_box_min = _box_min + position;
    ofVec3f new_box_max = _box_max + position;
    
    // Create new box
    _box = Box(Vector3(new_box_min.x, new_box_min.y, new_box_min.z), Vector3(new_box_max.x, new_box_max.y, new_box_max.z));
}

/* Get the model's position vector.
 *
 * @return {ofVec3f} The position vector of the model.
 */
ofVec3f Model::getPosition() {
    return _model.getPosition();
}

/* Returns reference to the model's model object.
 *
 * @return {ofxAssimpModelLoader &} Returns the model's _model member variable.
 */
ofxAssimpModelLoader &Model::getModel() {
    return _model;
}

/* Returns reference to the model's translated bounding box.
 *
 * @return {Box &} Returns the model's _box member variable.
 */
Box &Model::getBoundingBox() {
    return _box;
}

/* Draws the model using the default mode. */
void Model::draw() {
    draw(0);
}

/* Draws the model based on the desired drawing mode.
 *
 * @param {int} mode The drawing mode to use. Modes: {0: Shaded faces, 1: Wireframe, 2: Vertex/Point}
 */
void Model::draw(int mode) {
    drawMesh(_model, mode);
}

/* Draws the model's bounding box. */
void Model::drawBox() {
    ::drawBox(_box);
}
