//
//  MotionPath.cpp
//  CS235-TerrainProject
//
//  Created by William Nguyen on 5/9/18.
//

#include <cmath>
#include "MotionPath.hpp"
#include "Helpers.hpp"
#include "ray.h"
#include "vector3.h"

MotionPath::MotionPath() {
    speed = 0.0;
    targetSpeed = 0.1;
    totalPathLength = 0.0;
    traversalMode = 0;
    pathIsSet = false;
    tree = NULL;
    hiddenPoint = -1;
    
    resetState();
}
MotionPath::~MotionPath() {
}

/* Draws both the path and the points of the defined motion path */
void MotionPath::draw() {
    drawPath();
    drawPoints();
}

/* Draws only the path (a line/curve) of the defined motion path */
void MotionPath::drawPath() {
    ofNoFill();
    ofSetColor(ofColor::white);
    
    // Make sure the path is set before drawing
    setPath();
    
    path.draw();
}

/* Draws only the vertices of the defined motion path */
void MotionPath::drawPoints() {
    ofNoFill();
    ofSetColor(ofColor::white);
    
    int size = points.size();
    for (int i = 0; i < size; ++i) {
        // If point is set to hidden, then don't draw it
        if (i == hiddenPoint) {  continue; }
        
        // Draw a sphere around the point
        ofDrawSphere(points[i], 0.1);
    }
}

/* Provide a reference to the desired octree to ensure path can be mapped onto the terrain.
 *
 * @param {Octree *} octree The octree to reference when calculating the path.
 */
void MotionPath::useOctree(Octree *octree) {
    tree = octree;
}

/* Creates a path from a given list of points.
 *
 * Note that this function is destructive. The previous path state is wiped clean
 * and cannot be recovered.
 *
 * @param {const vector<ofVec3f> &} Reference to desired list of path points to generate a new path.
 */
void MotionPath::createPathFromList(const vector<ofVec3f> &pathPoints) {
    // Flag as path not set
    pathIsSet = false;
    
    // Empty the points list and copy over the points from pathPoints.
    points.clear();
    points = pathPoints;
}

/* Add a point to the motion path.
 *
 * @param {const ofVec3f &} point The point to add to the list of points.
 */
void MotionPath::addPoint(const ofVec3f &point) {
    // Flag as path not set
    pathIsSet = false;
    
    // Add point to point list
    points.push_back(point);
}

/* Updates an already defined point in the motion path.
 *
 * @param {int} index The index of the point in the points list that needs updating.
 * @param {const ofVec3f &} point The new point value to save.
 * @param {bool} Returns boolean stating whether the edit was successful or not.
 */
bool MotionPath::editPoint(int index, const ofVec3f &point) {
    // Flag as path not set
    pathIsSet = false;
    
    // If the index is out of bounds, print error and return false
    if (index < 0 || index >= points.size()) {
        cout << "ERROR: Cannot edit outside the bounds of the vector. Saw index " << index << " when the vector size is " << points.size() << endl;
        return false;
    }
    
    // Otherwise, update the point and return true
    points[index] = point;
    return true;
}

/* Clears (remove all points on) the motion path. */
void MotionPath::clear() {
    points.clear();
    path.clear();
    pathIsSet = true;
}

/* Delete the point at the defined index.
 *
 * @param {int} index The index of the point in the points list to erase.
 */
void MotionPath::deletePointAtIndex(int index) {
    // Cannot erase outside the bounds of the vector
    if (index < 0 || index >= points.size()) { return; }
    
    // Erase the point
    points.erase(points.begin() + index);
    
    // Flag as path not set
    pathIsSet = false;
}

/* Check to see if one of the motion path points were selected.
 *
 * @param {const Ray &} ray The ray used to determine intersection/selection of path points.
 * @return {int} The index of the selected path point. -1 means no path point selected.
 */
int MotionPath::selectWithRay(const Ray &ray) {
    int selectedIndex = -1;
    double selectedDistance = DBL_MAX;
    
    int numPoints = points.size();
    for (int i = 0; i < numPoints; ++i) {
        // Get plane of the point
        ofVec3f &p = points[i];
        Vector3 o = ray.origin;
        ofVec3f pointPlaneNorm = convertVec2ofVec(o) - p;
        
        // Get intersection point
        ofVec3f intersection;
        rayIntersectPlane(ray, p, pointPlaneNorm, intersection);
        
        // Determine if intersection point is within 0.1 radius
        if (distanceBetween(intersection, p) <= 0.1) {
            double distance = distanceBetween(o, intersection);
            
            // If intersection distance is closer than previously seen,
            // then the point is our new selected point
            if (distance < selectedDistance) {
                selectedIndex = i;
                selectedDistance = distance;
            }
        }
    }
    
    return selectedIndex;
}

/* Returns the point at the requested index.
 *
 * @param {int} index The index of the desired point.
 * @return {ofVec3f} The desired point.
 */
ofVec3f &MotionPath::getPoint(int index) {
    return points.at(index);
}

/* Hide the point at the target index in the vector.
 *
 * @param {int} index The index to hide during draws.
 */
void MotionPath::hideIndex(int index) {
    hiddenPoint = index;
}

/* Unhides the hidden point. */
void MotionPath::unhide() {
    hiddenPoint = -1;
}

/* Set the speed of the animation.
 *
 * @param {double} speed The desired traversal speed in length-units per second.
 */
void MotionPath::setSpeed(double desiredSpeed) {
    targetSpeed = desiredSpeed;
}

/* Set the traversal mode of the animation.
 *
 * The possible modes are {
 *     0: no looping (default),
 *     1: loop forwards continuously,
 *     2: loop forwards and backwards continuously
 * }. Otherwise, default mode 0 (no looping) will be used.
 *
 * @param {int} mode The traversal mode.
 */
void MotionPath::setTraversalMode(int mode) {
    if (mode >= 0 && mode < 3) {
        traversalMode = mode;
    } else {
        // If the mode is not one of the defined modes, then print out warning and use the default mode.
        cout << "WARNING: The desired mode is not defined. Using default (no looping) model." << endl;
        
        traversalMode = 0;
    }
}

/* Sets the traversal mode to 0 or no looping mode. */
void MotionPath::setNoLooping() {
    traversalMode = 0;
    
    // Ensure only forward movement
    moveBackward = false;
}

/* Sets the traversal mode to 1 or forward looping mode. */
void MotionPath::setForwardLooping() {
    traversalMode = 1;
    
    // Ensure only forward movement
    moveBackward = false;
}

/* Sets the traversal mode to 2 or forward and backward looping mode. */
void MotionPath::setForwardBackwardLooping() {
    traversalMode = 2;
}

/* Move the (imaginary) object to its next position on the path.
 *
 * Updates the traversed length of the object. Traversed length can be mapped
 * to world coordinates with ofPolyline. Depending on the mode, the object may
 * loop continously through the curve.
 *
 * @param {double} delta_time The time elapsed in seconds.
 */
void MotionPath::moveByDeltaTime(double delta_time) {
    // Set the path if it isn't set yet
    setPath();
    
    // If traversal needs to be restarted, then start at traversedLength = 0.0
    if (restartTraversal) {
        traversedLength = 0.0;
        restartTraversal = false;
    } else {
        // Update the object's speed
        speed = targetSpeed; // TODO: Update speed to provide ease-in-out animation
        
        // Move the object forward or backward by calculated distance
        double distance = speed * delta_time;
        if (moveBackward) {
            traversedLength -= distance;
        } else {
            traversedLength += distance;
        }
        
        // If the traversedLength goes beyond the path length, determine how to loop
        // if traversalMode is set to looping
        if (traversedLength > totalPathLength) {
            if (traversalMode == 0) {
                // No looping
                traversedLength = min(traversedLength, totalPathLength);
            } else if (traversalMode == 1) {
                // Forward looping only
                // Calculate the car's position since it might not be at length 0.0
                traversedLength = fmod(traversedLength, totalPathLength);
            } else if (traversalMode == 2) {
                // Forward and backward looping
                // Move the car backward
                moveBackward = true;
                
                // Calculate the car's position once it goes past the endpoint
                traversedLength = fmod(traversedLength, totalPathLength);
                traversedLength = totalPathLength - traversedLength;
            }
        } else if (moveBackward && traversedLength < 0) {
            // Move the car forward
            moveBackward = false;
            
            // Calculate the car's position once it goes past the endpoint
            traversedLength *= -1;
        }
    }
}

/* Restart the state of the animation. */
void MotionPath::resetState() {
    speed = 0.0;
    restartTraversal = true;
    traversedLength = 0.0;
    moveBackward = false;
}

/* Returns the points vector which defines the motion path.
 *
 * @return {const vector<ofVec3f> &} Reference to the list of path points.
 */
const vector<ofVec3f> &MotionPath::getPoints() {
    return points;
}

/* Returns the (position, lookat, up) vectors of the object's current state.
 *
 * @return {vector<ofVec3f>} The object's current state: (position, lookat, up) vectors.
 */
vector<ofVec3f> MotionPath::getPosLookatUp() {
    vector<ofVec3f> current_state;
    getPosLookatUp(current_state);
    return current_state;
}

/* Returns the (position, lookat, up) vectors of the object's current state.
 *
 * This method is created to avoid unnecessary copying of the vector object. The determined
 * position, lookat, and up vectors are automatically pushed onto the passed in vector.
 *
 * @param {vector<ofVec3f> &} store The reference vector in which to store the object's
 *                                  current state as (position, lookat, up) vectors.
 */
void MotionPath::getPosLookatUp(vector<ofVec3f> &store) {
    store.clear();
    store.reserve(3);
    
    double index = path.getIndexAtLength(traversedLength);
    
    // Get the Position vector
    ofVec3f position = path.getPointAtIndexInterpolated(index);
    store.push_back(position);
    
    // Calculate the Look At point
    ofVec3f tangent = path.getTangentAtIndexInterpolated(index);
    store.push_back(position + tangent);
    
    // Get the Up vector
    // Calculate and set the right vector before getting up vector
    ofVec3f right_vector = tangent.cross(ofVec3f(0, 1, 0));
    right_vector.normalize();
    path.setRightVector(right_vector);
    ofVec3f up = path.getNormalAtIndexInterpolated(index);
    store.push_back(up);
}

/* Get the current position of the object on the path.
 *
 * @return {ofVec3f} The current state's position vector.
 */
ofVec3f MotionPath::getPosition() {
    return path.getPointAtLength(traversedLength);
}

/* Get the current lookat point of the object.
 *
 * The lookat point is determined by adding a unit of the normalized tangent vector to
 * the current position.
 *
 * @return {ofVec3f} The current state's lookat/target point.
 */
ofVec3f MotionPath::getLookAt() {
    double index = path.getIndexAtLength(traversedLength);
    
    ofVec3f position = path.getPointAtIndexInterpolated(index);
    ofVec3f tangent = path.getTangentAtIndexInterpolated(index);
    return position + tangent;
}

/* Get the current lookat direction (tangent) of the object.
 *
 * @return {ofVec3f} The current state's lookat/target point.
 */
ofVec3f MotionPath::getLookAtDir() {
    double index = path.getIndexAtLength(traversedLength);
    ofVec3f tangent = path.getTangentAtIndexInterpolated(index);
    return tangent;
}

/* Get the current up vector of the object.
 *
 * @return {ofVec3f} The current state's up vector.
 */
ofVec3f MotionPath::getUp() {
    double index = path.getIndexAtLength(traversedLength);
    
    // Calculate and set a right vector using temporary (0, 1, 0) up vector
    ofVec3f tangent = path.getTangentAtIndexInterpolated(index);
    ofVec3f right_vector = tangent.cross(ofVec3f(0, 1, 0));
    right_vector.normalize();
    path.setRightVector(right_vector);
    
    // Get actual up vector from the path
    ofVec3f up = path.getNormalAtIndexInterpolated(index);
    return up;
}

/* Get the current theta/heading angle.
 *
 */
double MotionPath::getTheta() {
    double index = path.getIndexAtLength(traversedLength);
    
    ofVec3f rotation_axis(0, 1, 0); // Up vector is the rotation axis
    ofVec3f default_forward(0, 0, 1); // Fine tune this later
    ofVec3f tangent;
    tangent = path.getTangentAtIndexInterpolated(index);

    tangent.y = 0; // look only at the y=0 plane
    tangent.normalize();

    // Get the angle between forward and tangent vectors
    double theta = default_forward.angle(tangent);

    // Determine if theta should be positive or negative
    ofVec3f plane_norm = default_forward.cross(tangent);
    double direction = plane_norm.dot(rotation_axis);
    if (direction < 0) {
        theta *= -1;
    }
    
    return theta;
}

/* Get the current phi/upward angle.
 *
 */
double MotionPath::getPhi() {
    double index = path.getIndexAtLength(traversedLength);
    
    // Get and set the right vector
    ofVec3f tangent = path.getTangentAtIndexInterpolated(index);;
    ofVec3f right_vector = tangent.cross(ofVec3f(0, 1, 0)); // the rotation axis
    right_vector.normalize();
    path.setRightVector(right_vector);
    
    // Map tangent to horizontal plane in order to calculate upwards angle
    ofVec3f horizontal_tangent(tangent);
    horizontal_tangent.y = 0;
    horizontal_tangent.normalize();
    
    // Get the angle between horizontal tangent and tangent vectors
    double phi = horizontal_tangent.angle(tangent);
    
    // Determine if phi should be positive or negative
    ofVec3f plane_norm = horizontal_tangent.cross(tangent);
    double direction = plane_norm.dot(right_vector);
    if (direction < 0) {
        phi *= -1;
    }
    
    return phi;
}

/* Get the right vector for phi/upwards rotation.
 *
 */
ofVec3f MotionPath::getRightVector() {
    return path.getRightVector();
}

/* Get the rotation from the polyline curve.
 *
 */
ofVec3f MotionPath::getRotationAxis() {
    double index = path.getIndexAtLength(traversedLength);
    
    // Calculate and set a right vector using temporary (0, 1, 0) up vector
    ofVec3f tangent = path.getTangentAtIndexInterpolated(index);
    ofVec3f right_vector = tangent.cross(ofVec3f(0, 1, 0));
    right_vector.normalize();
    path.setRightVector(right_vector);
    
    return path.getRotationAtIndexInterpolated(index);
}

/* Get the rotation angle from the polyline curve.
 *
 */
double MotionPath::getRotationAngle() {
    double index = path.getIndexAtLength(traversedLength);
    return path.getAngleAtIndexInterpolated(index);
}

/* Create ofPolyline path from the points.
 *
 * The path created will be a curved path using ofPolyline.curveTo() which implements the
 * Catmull-Rom algorithm. Path will be generated twice: first pass is to get the naive curve,
 * second pass is to map the points onto the terrain by using the Octree object.
 */
void MotionPath::setPath() {
    // If path is set, then don't need to do anything
    if (pathIsSet) { return; }
    
    // First pass: Create a polyline curve
    generateCurve(points, path);
//    path = path.getSmoothed(3.0);
    
    // Update totalPathLength
    totalPathLength = path.getLengthAtIndex(path.size() - 1);
    
    // Second pass: Create a new polyline curve that correctly maps to the terrain as defined
    // by Octree *tree. If tree is null, then cannot do second pass.
    if (tree != NULL) {
        vector<ofVec3f> terrainMappedPoints;
        mapPointsToTerrain(path.getVertices(), terrainMappedPoints);
        generateCurve(terrainMappedPoints, path);
        path = path.getSmoothed(3);
    }

    // Update totalPathLength
    totalPathLength = path.getLengthAtIndex(path.size() - 1);
    
    // Set pathIsSet flag to true to avoid unnecessarily resetting the path
    pathIsSet = true;
}

/* Returns a ofPolyline defined by the list of points.
 *
 * @param {const vector<ofVec3f> &} vertices The list of vertices from which to generate a polyline curve.
 * @return {ofPolyline} The polyline curve that was created from the vertex list.
 */
ofPolyline MotionPath::generateCurve(const vector<ofVec3f> &vertices) {
    ofPolyline newPath;
    generateCurve(vertices, newPath);
    return newPath;
}

/* Returns a ofPolyline defined by the list of points.
 *
 * @param {const vector<ofVec3f> &} vertices The list of vertices from which to generate a polyline curve.
 * @param {ofPolyline &} newPath The reference used to return the generated polyline curve.
 */
void MotionPath::generateCurve(const vector<ofVec3f> &vertices, ofPolyline &newPath) {
    // Make sure the newPath is indeed a newPath by clearing it
    newPath.clear();
    
    // Determine the best method to use when generating the curve
    int size = vertices.size();
    if (size <= 2) { // 2 points can only define a line
        for (const auto &vertex : vertices) {
            newPath.lineTo(vertex);
        }
    } else {
        // Need to calculate a before-start and an after-end vertex to ensure all defined points are
        // in the curve. The curveTo() function always ignores the first and last added point since they're
        // typically only used for defining the initial and final point curves.
        ofVec3f direction;

        direction = vertices[0] - vertices[1];
        direction.normalize();
        ofVec3f before_start = vertices[0] + direction;

        direction = vertices[size-1] - vertices[size-2];
        direction.normalize();
        ofVec3f after_end = vertices[size-1] + direction;

        // Add before_start, all vertices, and after_end to path
        newPath.curveTo(before_start);
        for (const auto &vertex : vertices) {
            newPath.curveTo(vertex);
        }
        newPath.curveTo(after_end);
    }
    
//    for (const auto &vertex : vertices) {
//        newPath.lineTo(vertex);
//    }
}

/* Returns the list of vertices mapped onto the terrain.
 *
 * @param {const vector<ofVec3f> &} pathPoints The points already defined by the path.
 * @param {vector<ofVec3f> &} The reference used to return the mapped points of the polyline curve.
 */
void MotionPath::mapPointsToTerrain(const vector<ofVec3f> &pathPoints, vector<ofVec3f> &mappedPoints) {
    // Make sure the mappedPoints vector is empty
    mappedPoints.clear();
    
    ofVec3f direction(0, -1, 0);
    ofVec3f prevPoint(-1000, -1000, -1000); // intialize to arbitrary point (can be any point not on terrain)
//    for (const ofVec3f &point : pathPoints) {
//        // Create a ray to find the desired vertex on the terrain
//        ofVec3f origin = point + ofVec3f(0, 100, 0);
//        Ray ray = createRay(origin, direction);
//        int index = tree->selectWithRayNaive(ray);
//
//        // Check that index is valid
//        if (index == -1) { continue; }
//        // Check that vertex wasn't just added
//        ofVec3f vertex = tree->getVertexAtIndex(index);
//        if (vertex == prevPoint) { continue; }
//
//        // Get the vertex and add to mappedPoints
//        prevPoint = vertex;
//        mappedPoints.push_back(vertex);
//    }
    
    double length = 0.0;
    double delta_length = min(0.2, totalPathLength/10);
    do {
        // Get point on path
        ofVec3f point = path.getPointAtLength(length);
        ofVec3f origin = point + ofVec3f(0, 100, 0);
        Ray ray = createRay(origin, direction);
        
        // Map point to terrain
        int index = tree->selectWithRayNaive(ray);
        if (index != -1) {
            // Get vertex and add to mapped Points
            ofVec3f vertex = tree->getVertexAtIndex(index);
            if (vertex != prevPoint) {
                mappedPoints.push_back(vertex);
                prevPoint = vertex;
            }
        }
        
        // Increment length
        length += delta_length;
    } while (length < totalPathLength);
    
    return mappedPoints;
}



