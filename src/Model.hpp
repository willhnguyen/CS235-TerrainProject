//
//  Model.hpp
//  CS235-TerrainProject
//
//  The Model class is a wrapper to pair up ofxAssimpModelLoader and Box. This
//  was created to address the difficulty of moving both the rover's
//  corresponding bounding box.
//
//  Created by William Nguyen on 5/9/18.
//

#ifndef Model_hpp
#define Model_hpp

#include <stdio.h>
#include "ofxAssimpModelLoader.h"
#include "box.h"
#include "ray.h"

class Model {
public:
    Model();
    ~Model();
    
    // Setters
    void setModel(ofxAssimpModelLoader &, double);
    void setPosition(ofVec3f &);
    
    // Getters
    ofxAssimpModelLoader &getModel();
    Box &getBoundingBox();
    ofVec3f getPosition();
    
    // Draw methods
    void draw();
    void draw(int);
    void drawBox();
    
private:
    ofxAssimpModelLoader _model;
    ofVec3f _box_min, _box_max;
    Box _box;
};

#endif /* Model_hpp */
