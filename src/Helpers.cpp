//
//  Helpers.cpp
//  CS235-TerrainProject
//
//  This file contains helper functions that are used by the main app. Some
//  functions from the starter code have been migrated here to organize the code
//  for readability.
//
//  Created by William Nguyen on 5/9/18.
//

#include "Helpers.hpp"
#include <vector>
#include <cmath>

/* Visualize the provided bounding box in 3D space.
 *
 * @param {const Box &} box The bounding box that should be drawn.
 */
void drawBox(const Box &box) {
    ofNoFill();
    ofSetColor(ofColor::white);
    
    Vector3 min = box.parameters[0];
    Vector3 max = box.parameters[1];
    Vector3 size = max - min;
    Vector3 center = size / 2 + min;
    ofVec3f p = ofVec3f(center.x(), center.y(), center.z());
    float w = size.x();
    float h = size.y();
    float d = size.z();
    ofDrawBox(p, w, h, d);
}

/* Generate the bounding box for the provided mesh.
 *
 * @param {const ofMesh &} mesh The mesh to calculate the bounding box.
 * @return {Box} The bounding box of the mesh.
 */
Box meshBounds(const ofMesh & mesh) {
    int n = mesh.getNumVertices();
    ofVec3f v = mesh.getVertex(0);
    ofVec3f max = v;
    ofVec3f min = v;
    for (int i = 1; i < n; i++) {
        ofVec3f v = mesh.getVertex(i);
        
        if (v.x > max.x) max.x = v.x;
        else if (v.x < min.x) min.x = v.x;
        
        if (v.y > max.y) max.y = v.y;
        else if (v.y < min.y) min.y = v.y;
        
        if (v.z > max.z) max.z = v.z;
        else if (v.z < min.z) min.z = v.z;
    }
    return Box(Vector3(min.x, min.y, min.z), Vector3(max.x, max.y, max.z));
}

/* Generate the bounding box for the provided ofxAssimpModelLoader.
 *
 * @param {const ofxAssimpModelLoader &} mesh The model to calculate the bounding box.
 * @return {Box} The bounding box of the mesh.
 */
Box meshBounds(ofxAssimpModelLoader &object, double scale) {
    int numMeshes = object.getMeshCount();
    
    // Return if the model has no mesh
    if (numMeshes == 0) { return Box(); }
    
    // Get all vertices of the object
    vector<ofVec3f> allVertices;
    for (int i = 0; i < numMeshes; ++i) {
        ofMesh mesh = object.getMesh(i);
        for (ofVec3f &vertex : mesh.getVertices()) {
            allVertices.push_back(vertex);
        }
    }
    
    // Use vertices to determine the bounding box
    ofVec3f bounding_max = allVertices[0];
    ofVec3f bounding_min = allVertices[0];
    for (ofVec3f &vertex : allVertices) {
        bounding_max.x = max(bounding_max.x, vertex.x);
        bounding_max.y = max(bounding_max.y, vertex.y);
        bounding_max.z = max(bounding_max.z, vertex.z);
        
        bounding_min.x = min(bounding_min.x, vertex.x);
        bounding_min.y = min(bounding_min.y, vertex.y);
        bounding_min.z = min(bounding_min.z, vertex.z);
    }
    
    // Scale the box
    bounding_max *= scale;
    bounding_min *= scale;
    
    // Reposition box
    bounding_max += object.getPosition();
    bounding_min += object.getPosition();
    
    return Box(Vector3( bounding_min.x, bounding_min.y, bounding_min.z ), Vector3( bounding_max.x, bounding_max.y, bounding_max.z ));
}

/* Subdivide a box into 8 equally-sized octant boxes.
 *
 * @param {const Box &} box The main box to divide.
 * @return {vector<Box> &} boxList Container to store the 8 generated octant boxes.
 */
void subDivideBox8(const Box &box, vector<Box> & boxList) {
    Vector3 min = box.parameters[0];
    Vector3 max = box.parameters[1];
    Vector3 size = max - min;
    Vector3 center = size / 2 + min;
    float xdist = (max.x() - min.x()) / 2;
    float ydist = (max.y() - min.y()) / 2;
    float zdist = (max.z() - min.z()) / 2;
    Vector3 h = Vector3(0, ydist, 0);
    
    //  generate ground floor
    Box b[8];
    b[0] = Box(min, center);
    b[1] = Box(b[0].min() + Vector3(xdist, 0, 0), b[0].max() + Vector3(xdist, 0, 0));
    b[2] = Box(b[1].min() + Vector3(0, 0, zdist), b[1].max() + Vector3(0, 0, zdist));
    b[3] = Box(b[2].min() + Vector3(-xdist, 0, 0), b[2].max() + Vector3(-xdist, 0, 0));
    
    // Reserve 8 slots since we're guaranteed 8 octants.
    boxList.clear();
    boxList.reserve(8);
    for (int i = 0; i < 4; i++)
        boxList.push_back(b[i]);
    
    // generate second story
    for (int i = 4; i < 8; i++) {
        b[i] = Box(b[i - 4].min() + h, b[i - 4].max() + h);
        boxList.push_back(b[i]);
    }
}

/* Draw the XYZ axis in RGB at the specified location.
 *
 * @param {ofVec3f} location The reference point in which to draw the axes.
 */
void drawAxis(ofVec3f location) {
    ofPushMatrix();
    ofTranslate(location);
    
    ofSetLineWidth(1.0);
    
    // X Axis (Red)
    ofSetColor(ofColor(255, 0, 0));
    ofDrawLine(ofPoint(0, 0, 0), ofPoint(1, 0, 0));
    
    // Y Axis (Green)
    ofSetColor(ofColor(0, 255, 0));
    ofDrawLine(ofPoint(0, 0, 0), ofPoint(0, 1, 0));
    
    // Z Axis (Blue)
    ofSetColor(ofColor(0, 0, 255));
    ofDrawLine(ofPoint(0, 0, 0), ofPoint(0, 0, 1));
    
    ofPopMatrix();
}

/* Draw the mesh in the desired mode (shaded, wireframe, vertex/point).
 *
 * @param {ofxAssimpModelLoader &} mesh The object/meshes to draw.
 * @param {int} mode The drawing mode to use.
 */
void drawMesh(ofxAssimpModelLoader &object, int mode) {
    if (mode == 1) { // Wireframe mode
        ofDisableLighting();
        ofSetColor(ofColor::slateGray);
        object.drawWireframe();
    } else if (mode == 2) { // Vertex/Point mode
        ofDisableLighting();
        glPointSize(1);
        ofSetColor(ofColor::green);
        object.drawVertices();
    } else { // Shaded face mode
        ofEnableLighting();
        object.drawFaces();
    }
}

/* Calculate/Interpolate a point that is some percentage between two points.
 *
 * @param {double} percentage The percent value between 0 and 1 to interpolate between the two points.
 * @param {ofVec3f &} pointA The start point at percentage value 0.0.
 * @param {ofVec3f &} pointB The end point at percentage value 1.0.
 */
ofVec3f interpolate(double percentage, ofVec3f &pointA, ofVec3f &pointB) {
    return (pointA * (1.0 - percentage)) + (pointB * percentage);
}

/* Create a ray using ofVec3f instead of Vector3.
 *
 * @param {const ofVec3f &} o The origin point of the new ray.
 * @param {const ofVec3f &} d The direction of the new ray.
 * @param {Ray} The ray generated with the provided origin and direction vectors.
 */
Ray createRay(ofVec3f &o, ofVec3f &d) {
    return Ray(Vector3(o.x, o.y, o.z), Vector3(d.x, d.y, d.z));
}

/* Convert between ofVec3f and Vector3 */
Vector3 convertofVec2Vec(ofVec3f &v) {
    return Vector3(v.x, v.y, v.z);
}
/* Convert between Vector3 and ofVec3f */
ofVec3f convertVec2ofVec(Vector3 &v) {
    return ofVec3f(v.x(), v.y(), v.z());
}

//---------------------------------------------------------------
// test if a ray intersects a plane.  If there is an intersection,
// return true and put point of intersection in "point"
//
bool rayIntersectPlane(const ofVec3f &rayPoint, const ofVec3f &raydir, const ofVec3f &planePoint,
                       const ofVec3f &planeNorm, ofVec3f &point)
{
    // if d1 is 0, then the ray is on the plane or there is no intersection
    //
    const float eps = .000000001;
    float d1 = (planePoint - rayPoint).dot(planeNorm);
    if (abs(d1) < eps) return false;
    
    //  if d2 is 0, then the ray is parallel to the plane
    //
    float d2 = raydir.dot(planeNorm);
    if (abs(d2) < eps) return false;
    
    //  compute the intersection point and return it in "point"
    //
    point = (d1 / d2) * raydir + rayPoint;
    return true;
}
bool rayIntersectPlane(const Ray &ray, ofVec3f const &planePoint, const ofVec3f &planeNorm, ofVec3f &point) {
    
    Vector3 o = ray.origin;
    Vector3 d = ray.direction;
    return rayIntersectPlane(
                             convertVec2ofVec(o),
                             convertVec2ofVec(d),
                             planePoint,
                             planeNorm,
                             point);
}

// Compute the reflection of a vector incident on a surface at the normal.
//
//
ofVec3f reflectVector(const ofVec3f &v, const ofVec3f &n) {
    return (v - 2 * v.dot(n) * n);
}


/***************************************************************************************************
 * Box Mid Methods
 ****************************************************************************************************/
Vector3 getBoxMid(Box &b) {
    Vector3 minBound = b.min();
    Vector3 maxBound = b.max();
    double midx = (minBound.x() + maxBound.x()) / 2;
    double midy = (minBound.y() + maxBound.y()) / 2;
    double midz = (minBound.z() + maxBound.z()) / 2;
    
    return Vector3(midx, midy, midz);
}

/***************************************************************************************************
 * Distance Methods
 ****************************************************************************************************/
double distanceBetween(Box &a, Box &b) {
    return distanceBetween(getBoxMid(a), getBoxMid(b));
}
double distanceBetween(Box &a, const ofVec3f &b) {
    return distanceBetween(getBoxMid(a), b);
}
double distanceBetween(Box &a, const Ray &b) {
    return distanceBetween(getBoxMid(a), b.origin);
}
double distanceBetween(Box &a, const Vector3 &b) {
    return distanceBetween(getBoxMid(a), b);
}

double distanceBetween(const Vector3 &a, Box &b) {
    return distanceBetween(a, getBoxMid(b));
}
double distanceBetween(const Vector3 &a, const ofVec3f &b) {
    return sqrt(pow(b.x - a.x(), 2) + pow(b.y - a.y(), 2) + pow(b.z - a.z(), 2));
}
double distanceBetween(const Vector3 &a, const Ray &b) {
    return distanceBetween(a, b.origin);
}
double distanceBetween(const Vector3 &a, const Vector3 &b) {
    return sqrt(pow(b.x() - a.x(), 2) + pow(b.y() - a.y(), 2) + pow(b.z() - a.z(), 2));
}

double distanceBetween(const ofVec3f &a, Box &b) {
    return distanceBetween(a, getBoxMid(b));
}
double distanceBetween(const ofVec3f &a, const ofVec3f &b) {
    return sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2) + pow(b.z - a.z, 2));
}
double distanceBetween(const ofVec3f &a, const Ray &b) {
    return distanceBetween(a, b.origin);
}
double distanceBetween(const ofVec3f &a, const Vector3 &b) {
    return sqrt(pow(b.x() - a.x, 2) + pow(b.y() - a.y, 2) + pow(b.z() - a.z, 2));
}

double distanceBetween(const Ray &a, Box &b) {
    return distanceBetween(a, getBoxMid(b));
}
double distanceBetween(const Ray &a, const ofVec3f &b) {
    Vector3 b1(b.x, b.y, b.z);
    return distanceBetween(a, b1);
}
//double distanceBetween(const Ray &a, const Ray &b) {
//    return distanceBetween(a.origin, b.origin);
//}
double distanceBetween(const Ray &a, const Vector3 &b) {
    Vector3 o = a.origin;
    Vector3 d = a.direction;
    
    Vector3 closestPointOnRay = o + d * (d * (b - o));
    
    return distanceBetween(a.origin, b);
}
