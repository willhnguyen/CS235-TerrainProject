
//--------------------------------------------------------------
//
//  Kevin M. Smith
//
//  Mars HiRise Project - startup scene
// 
//  This is an openFrameworks 3D scene that includes an EasyCam
//  and example 3D geometry which I have reconstructed from Mars
//  HiRis photographs taken the Mars Reconnaisance Orbiter
//
//  You will use this source file (and include file) as a starting point
//  to implement assignment 5  (Parts I and II)
//
//  Please do not modify any of the keymappings.  I would like 
//  the input interface to be the same for each student's 
//  work.  Please also add your name/date below.

//  Please document/comment all of your work !
//  Have Fun !!
//
//  Student Name:   William H. Nguyen
//  Date: Wednesday, May 9, 2018


#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include "ofApp.h"
#include "Helpers.hpp"

#define VERBOSEFLAG false

ofApp::~ofApp() {
    // Free up the memory
    delete octreeMars;
}

/* Initialize the scene & lighting & states, and load terrain mesh. */
void ofApp::setup(){
    currMicrosecs = ofGetElapsedTimeMicros();
    
//    // Window size setup
//    ofSetWindowShape(1024, 768);
    
//    ofBackgroundGradient(ofColor(20), ofColor(0));   // pick your own background
//    cout << ofGetFrameRate() << endl;
    
    // Set the background color
    ofBackground(ofColor::black);
    
    // Set up the camera
    cam.setDistance(10);
    cam.setNearClip(.1);
    cam.setFov(65.5);   // approx equivalent to 28mm in 35mm format
    ofSetVerticalSync(true);
    cam.disableMouseInput();
    ofEnableSmoothing();
    ofEnableDepthTest();
    
    // Set up the lighting
    initLightingAndMaterials();
    
    // Load the mars object
    ofxAssimpModelLoader marsObj;
    marsObj.loadModel("geo/mars-low-v2.obj");
    marsObj.setScaleNormalization(false);
    mars.setModel(marsObj, 1.0);
    
    // Initialize rover state
    bRoverLoaded = false;
    
    // Initialize keypress states
    bAltKeyDown = false;
    bCtrlKeyDown = false;
    
    // Initialize drawing properties
    drawingMode = 0; // Face mode
    
    // Initialize selection properties
    selectedModel = 1; // Terrain selected
    bPointSelectionMode = false;
    bTerrainPointSelected = false;
    
    // Generate the Octree for the Mars terrain mesh
    double elapsed_time = ofGetElapsedTimeMillis();
    octreeMars = new Octree(mars.getModel().getMesh(0));
    elapsed_time = ofGetElapsedTimeMillis() - elapsed_time;
    
    cout << "Octree generated. Took " << elapsed_time << " milliseconds" << endl;
    
    // Initialize motion path properties
    bMotionPathSelectionMode = false;
    mPath.useOctree(octreeMars);
    mPath.resetState();
    mPath.setNoLooping();
    mPathSelectedIndex = -1;
    mPathSelected = false;
    
    // Initialize aniamtion properties
    bPlay = false;
    rover.getModel().setRotation(0, 0, 1, 0, 0);
    
    // Initialize GUI
    gui.setup();
    gui.add(speedSlider.setup("Speed", 0.1, 0.0, 10.0));
    gui.add(cam0.setup("Reset Cam"));
    gui.add(cam1.setup("Rover Cam"));
    gui.add(cam2.setup("Tracking Cam"));
    gui.add(cam3.setup("Follow Cam"));
    gui.add(cam4.setup("Rear Cam"));
    gui.setPosition(20, 40);
    
    cam0.addListener(this, &ofApp::resetCam);
    cam1.addListener(this, &ofApp::chooseRoverCam);
    cam2.addListener(this, &ofApp::chooseTrackingCam);
    cam3.addListener(this, &ofApp::chooseFollowCam);
    cam4.addListener(this, &ofApp::chooseRearCam);
}

//--------------------------------------------------------------
// incrementally update scene (animation)
//
void ofApp::update() {
    if (bPlay && bRoverLoaded) {
        // Determine the delta time (in seconds) and store new time value
        uint64_t currTime = ofGetElapsedTimeMicros();
        double delta_time = (currTime - currMicrosecs) / 1e6;
        currMicrosecs = currTime;
        
        // Calculate movement
        mPath.moveByDeltaTime(delta_time);
        
        // Reposition the rover
        ofVec3f position = mPath.getPosition();
        rover.setPosition(position);
        
        // Rotate the rover
        double theta = mPath.getTheta();
//        double phi = mPath.getPhi();
//        ofVec3f right_vec = mPath.getRightVector();
//
//        cout << "Phi: " << phi << endl;
//
        rover.getModel().setRotation(0, theta, 0, 1, 0);
//        if (!isnan(phi)) {
//            rover.getModel().setRotation(1, phi, right_vec.x, right_vec.y, right_vec.z);
//        }
//        ofVec3f axis = mPath.getRotationAxis();
//        double angle = mPath.getRotationAngle();
//        rover.getModel().setRotation(0, angle, axis.x, axis.y, axis.z);
//
//        cout << "axis: " << axis << endl;        
        
        // Update camera
        if (camMode == 1) { // rover cam
            ofVec3f roverPosition = rover.getPosition() + ofVec3f(0, 0.7, 0);
            ofVec3f roverLookAtDir = mPath.getLookAtDir();
            cam.setPosition(roverPosition + (0.3 * roverLookAtDir));
            cam.lookAt(roverPosition + roverLookAtDir, ofVec3f(0, 1, 0));
        } else if (camMode == 2) { // tracking cam
            ofVec3f roverPosition = rover.getPosition();
            cam.setPosition(roverPosition + ofVec3f(0, 10, 0));
            cam.lookAt(roverPosition, ofVec3f(0, 0, -1));
        } else if (camMode == 3) { // follow cam
            ofVec3f roverPosition = rover.getPosition();
            ofVec3f roverLookAtDir = mPath.getLookAtDir();
            roverLookAtDir.y = 0;
            roverLookAtDir.normalize();
            cam.setPosition(roverPosition + (-1.5 * roverLookAtDir) + ofVec3f(0, 1, 0));
            cam.lookAt(roverPosition, ofVec3f(0, 1, 0));
        } else if (camMode == 4) { // rear cam
            ofVec3f roverPosition = rover.getPosition() + ofVec3f(0, 0.7, 0);
            ofVec3f roverLookAtDir = mPath.getLookAtDir();
            cam.setPosition(roverPosition + (-0.3 * roverLookAtDir));
            cam.lookAt(roverPosition - roverLookAtDir, ofVec3f(0, 1, 0));
        }
    }
    
    // Update the motion path speed based on gui slider
    mPath.setSpeed(speedSlider);
}
//--------------------------------------------------------------
void ofApp::draw(){
    
    // Set up the camera viewport and draw objects within the viewport
    ofEnableDepthTest();
	cam.begin();
	ofPushMatrix();

    // Draw Terrain and Rover
    mars.draw(drawingMode);
    if (bRoverLoaded) {
        rover.draw(drawingMode);
    }
    
//    // Draw cam
//    ofNoFill();
//    ofSetColor(ofColor::blue);
//    ofVec3f roverCamPosition = rover.getPosition() + ofVec3f(0, 0.7, 0) + (0.3 * mPath.getLookAtDir());
//    ofDrawSphere(roverCamPosition, 0.1);

    // Draw Axes for Selected Object
    if (!bPlay && selectedModel == 2 && bRoverLoaded) {
        drawAxis(rover.getModel().getPosition());
        rover.drawBox();
    }
    else if (!bPlay && selectedModel == 1) { drawAxis(ofVec3f(0, 0, 0)); }

	// Highlight the selected point
	if (bPointSelectionMode && bTerrainPointSelected) {
        ofNoFill();
		ofSetColor(ofColor::green);
		ofDrawSphere(selectedPoint, .1);
	}
    
    // Draw Path
    mPath.draw();
    
	ofPopMatrix();
	cam.end();
    
    // Draw Top Left Info Text
    ofDisableDepthTest();
    if (bPointSelectionMode && bTerrainPointSelected) {
        string vertex_info = "Point selected is (" +
        to_string(selectedPoint.x) + "," +
        to_string(selectedPoint.y) + "," +
        to_string(selectedPoint.z) + ")";
        ofDrawBitmapStringHighlight(vertex_info, 20, 20);
    }
    gui.draw();
}

void ofApp::keyPressed(int key) {

	switch (key) {
        case 'C':
        case 'c':
            if (cam.getMouseInputEnabled()) cam.disableMouseInput();
            else cam.enableMouseInput();
            break;
        case 'D':
        case 'd':
            deleteSelectedPathPoint();
            break;
        case 'F':
        case 'f':
            ofToggleFullscreen();
            break;
        case 'H':
        case 'h':
            break;
        case 'M':
        case 'm':
            toggleMotionPathSelectionMode();
            break;
        case 'O':
        case 'o':
            if (bCtrlKeyDown) {
                promptLoadMotionPath();
            }
            break;
        case 'P':
        case 'p':
            togglePointSelectionMode();
            break;
        case 'r':
//            cam.reset();
            resetCam();
            break;
        case 's':
            if (bCtrlKeyDown) {
                savePicture();
                promptSaveMotionPath();
            }
            break;
        case 't':
            setCameraTarget();
            break;
        case 'u':
            break;
        case 'v':
            togglePointsDisplay();
            break;
        case 'V':
            break;
        case 'w':
            toggleWireframeMode();
            break;
        case OF_KEY_ALT:
            cam.enableMouseInput();
            bAltKeyDown = true;
            break;
        case OF_KEY_CONTROL:
        case OF_KEY_COMMAND:
            bCtrlKeyDown = true;
            break;
        case OF_KEY_SHIFT:
            break;
        case OF_KEY_DEL:
        case OF_KEY_BACKSPACE:
            mPath.clear();
            break;
        case ' ':
            toggleAnimation();
            break;
        case OF_KEY_TAB:
            cycleCam();
            break;
        case '0':
            chooseCam(0);
            break;
        case '1':
            chooseCam(1);
            break;
        case '2':
            chooseCam(2);
            break;
        case '3':
            chooseCam(3);
            break;
        case '4':
            chooseCam(4);
            break;
        default:
            break;
	}
}
void ofApp::keyReleased(int key) {
    switch (key) {
        case OF_KEY_ALT:
            cam.disableMouseInput();
            bAltKeyDown = false;
            break;
        case OF_KEY_CONTROL:
        case OF_KEY_COMMAND:
            bCtrlKeyDown = false;
            break;
        case OF_KEY_SHIFT:
            break;
        default:
            break;
    }
}

void ofApp::toggleWireframeMode() {
    drawingMode = (drawingMode == 1 ? 0 : 1);
}
void ofApp::togglePointsDisplay() {
    drawingMode = (drawingMode == 2 ? 0 : 2);
}
void ofApp::toggleSelectTerrain() {
    selectedModel = (selectedModel == 1 ? 0 : 1);
}
void ofApp::togglePointSelectionMode() {
    bPointSelectionMode = !bPointSelectionMode;
    
    // Deselect point
    bTerrainPointSelected = false;
    if (!bPointSelectionMode && bMotionPathSelectionMode) {
        toggleMotionPathSelectionMode();
    }
}
void ofApp::toggleMotionPathSelectionMode() {
    // Turn motion path tool on
    cout << bMotionPathSelectionMode << endl;
    bMotionPathSelectionMode = !bMotionPathSelectionMode;
    mPathSelectedIndex = -1;
    cout << bMotionPathSelectionMode << endl;
    cout << bMotionPathSelectionMode << endl;
    
    // Turn off animation if selection mode is on
    if (bMotionPathSelectionMode) {
        bPlay = false;
    }
    
    // Make sure point Selection Mode is on
    if (bMotionPathSelectionMode && !bPointSelectionMode) {
        togglePointSelectionMode();
    } else if (!bMotionPathSelectionMode && bPointSelectionMode) {
        togglePointSelectionMode();
    }
    
    cout << bMotionPathSelectionMode << endl;
}
void ofApp::toggleAnimation() {
    bPlay = !bPlay;
    
    // Don't turn on animation if rover isn't selected
    if (bPlay && selectedModel != 2) {
        bPlay = false;
    }
    
    // Turn off motion path selection mode and point selection mode if playing
    if (bPlay) {
        mPath.resetState();
        
        if (bMotionPathSelectionMode) {
            toggleMotionPathSelectionMode();
        }
        if (bPointSelectionMode) {
            togglePointSelectionMode();
        }
    }
}


//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
}


//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {
    // Only perform selection if the left mouse button is clicked.
    
    if (!bAltKeyDown && button == 0) {
        Ray ray = getMouseRay();
        double closestDistance = DBL_MAX;
        double distance; // for calculations
        selectedModel = 0; // Set to none selected
        bTerrainPointSelected = false;
        
        // Check rover selection
        if (rover.getBoundingBox().intersect(ray, -100, 100)) {
            // Store the mouse's intersection point to properly move the model in the
            // scene without a jump on mouseDrag event. Stores position in tempPosition.
            tempRoverPosition = rover.getPosition();
            rayIntersectPlane(getMouseRay(), ofVec3f(0, 0, tempRoverPosition.z), cam.getZAxis(), tempPosition);
            
            // Check for closest selection
            distance = distanceBetween(ray, rover.getPosition());
            if (distance < closestDistance) {
                closestDistance = distance;
                
                bTerrainPointSelected = false;
                selectedModel = 2; // Set to rover selected
            }
        }
        
        // Check motion path selection
        mPathSelectedIndex = mPath.selectWithRay(ray);
        if (mPathSelectedIndex != -1) {
            // Get the selected point on the motion path
            selectedPoint = mPath.getPoint(mPathSelectedIndex);
            
            // Check for closest selection
            distance = distanceBetween(selectedPoint, ray);
            if (distance < closestDistance) {
                closestDistance = distance;
                
                // Only set bTerrainPointSelected to true if in point selection mode
                bTerrainPointSelected = bPointSelectionMode;
                selectedModel = 1; // Set to terrain selected
                
                mPathSelected = true;
                mPath.hideIndex(mPathSelectedIndex);
            }
        } else { // Check terrain selection
            if (doPointSelection(ray, VERBOSEFLAG)) {
                
                // Check for closest selection
                distance = distanceBetween(selectedPoint, ray);
                if (distance < closestDistance) {
                    closestDistance = distance;
                    
                    // Only set bTerrainPointSelected to true if in point selection mode
                    bTerrainPointSelected = bPointSelectionMode;
                    selectedModel = 1; // Set to terrain selected
                }
            }
        }
        
//        // Check terrain selection
//        bool terrainSelectionPossible = false;
//        if (mPathSelectedIndex == -1) { // If motion path is selected, then obviously don't select terrains
//            // Only set bTerrainPointSelected to true if in point selection mode
//            bTerrainPointSelected = bPointSelectionMode;
//            terrainSelectionPossible = true;
//        }
//
//        // Check which selection is most probable (closest)
//        if (roverSelectionPossible) {
//            double roverDistance =
//            closestDistance = min(closestDistance, distanceBetween(ray, rover.getPosition()));
//        }
//
//        // Check rover selection
//        if (rover.getBoundingBox().intersect(ray, -100, 100)) {
//            // Store the mouse's intersection point to properly move the model in the
//            // scene without a jump on mouseDrag event. Stores position in tempPosition.
//            tempRoverPosition = rover.getPosition();
//            rayIntersectPlane(getMouseRay(), ofVec3f(0, 0, tempRoverPosition.z), cam.getZAxis(), tempPosition);
//
//            bTerrainPointSelected = false;
//            selectedModel = 2;
//        } else if (doPointSelection(ray, VERBOSEFLAG)) {
//            // Only set bTerrainPointSelected to true if in point selection mode
//            bTerrainPointSelected = bPointSelectionMode;
//            selectedModel = 1;
//        } else {
//            bTerrainPointSelected = false;
//            selectedModel = 0;
//        }
    }
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {
    // Let the user drag the current point
    if (!bAltKeyDown && button == 0) {
        Ray ray = getMouseRay();
        if (bTerrainPointSelected) {
            // Try to select a new point on the terrain
            doPointSelection(ray, VERBOSEFLAG);
        } else if (!bPlay && selectedModel == 2) {
            // Do not permit rover movement if in play mode
            // Calculate mouse's intersection point to calculate how to translate
            // the model without a jump due to initial mouseDrag event.
            ofVec3f point;
            rayIntersectPlane(ray, ofVec3f(0, 0, tempRoverPosition.z), cam.getZAxis(), point);
            
            // Move the rover to new position
            point = tempRoverPosition + point - tempPosition;
            rover.setPosition(point);
        }
    }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {
    // If in motion path selection mode and a point was selected, then add point to motion
    // path or update motion path point
    if (!bAltKeyDown && button == 0 && bMotionPathSelectionMode && bTerrainPointSelected) {
        if (mPathSelected) { // Edit the path
            mPath.editPoint(mPathSelectedIndex, selectedPoint);
            mPath.unhide();
            mPathSelected = false;
        } else { // Append to path
            mPath.addPoint(selectedPoint);
            mPathSelectedIndex = mPath.getPoints().size() - 1;
        }
    }
}


/* Perform point selection on the Octree.
 *
 * @param {const Ray &} ray The ray used to determine bounding box intersections.
 * @param {bool} verbose Determines whether to log output to the console.
 * @return {bool} Value denoting whether a point was selected or not.
 */
bool ofApp::doPointSelection(const Ray &ray, const bool verbose) {
    uint64_t elapsed_time;
    
    // If verbose, then get time metrics as well
    if (verbose) {
        elapsed_time = ofGetElapsedTimeMicros();
//        selectedVertex = octreeMars->selectWithRay(ray);
        selectedVertex = octreeMars->selectWithRayNaive(ray);
        elapsed_time = ofGetElapsedTimeMicros() - elapsed_time;
    } else {
//        selectedVertex = octreeMars->selectWithRay(ray);
        selectedVertex = octreeMars->selectWithRayNaive(ray);
    }
    
    bool vertexSelected = selectedVertex != -1;
    
    // If a vertex has been selected, then fetch the selected vertex
    if (vertexSelected) {
        selectedPoint = mars.getModel().getMesh(0).getVertices()[selectedVertex];
    }
    
    // If verbose flag is set, then log metrics
    if (verbose) {
        if (vertexSelected) { cout << "Selected vertex index: " << selectedVertex; }
        else { cout << "No vertex selected."; }
        
        cout << " Took " << elapsed_time << " microseconds" << endl;
    }
    
    return vertexSelected;
}

/* Set the camera's target to either the selected point on the terrain or the rover. */
void ofApp::setCameraTarget() {
    if (selectedModel == 1 && bTerrainPointSelected) {
        cam.setTarget(selectedPoint);
    } else if (selectedModel == 2) {
        cam.setTarget(rover.getPosition());
    }
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

/* Initialize a single light source in OpenGL. */
void ofApp::initLightingAndMaterials() {

	static float ambient[] =
	{ .5f, .5f, .5, 1.0f };
	static float diffuse[] =
	{ 1.0f, 1.0f, 1.0f, 1.0f };
	static float position[] =
	{5.0, 5.0, 5.0, 0.0 };
	static float lmodel_ambient[] =
	{ 1.0f, 1.0f, 1.0f, 1.0f };
	static float lmodel_twoside[] =
	{ GL_TRUE };

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, position);

//    glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);
//    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
//    glLightfv(GL_LIGHT1, GL_POSITION, position);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE, lmodel_twoside);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
//    glEnable(GL_LIGHT1);
	glShadeModel(GL_SMOOTH);
} 

/* Save the display as a PNG image. */
void ofApp::savePicture() {
	ofImage picture;
	picture.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
	picture.save("screenshot.png");
	cout << "picture saved" << endl;
}

/* Load OBJ files during drag-and-drop event.
 *
 * OBJ files can be dragged-and-dropped into the viewport. The model
 * will be positioned in the viewport based on the mouse's screen-space
 * position and its equivalent intersection point at the z=0 plane.
 *
 * @param {ofDragInfo} dragInfo Information about the file dropped into the viewport.
 */
void ofApp::dragEvent(ofDragInfo dragInfo) {

    Ray ray = getMouseRay();
	ofVec3f point;
	rayIntersectPlane(ray, ofVec3f(0, 0, 0), cam.getZAxis(), point);

    ofxAssimpModelLoader roverObj;
    if (roverObj.loadModel(dragInfo.files[0])) {
        roverObj.setScaleNormalization(false);
        rover.setModel(roverObj, 0.005);
        rover.setPosition(point);
		bRoverLoaded = true;
        selectedModel = 2;
	}
	else cout << "Error: Can't load model" << dragInfo.files[0] << endl;
}

/* Calculate the Ray based on the mouse position translated from screen space to world space.
 *
 * @return {Ray} The ray from the camera origin to the mouse position in world space.
 */
Ray ofApp::getMouseRay() {
    ofVec3f rayPoint = cam.screenToWorld(ofVec3f(mouseX, mouseY));
    ofVec3f rayDir = rayPoint - cam.getPosition();
    rayDir.normalize();
    
    return Ray(Vector3(rayPoint.x, rayPoint.y, rayPoint.z), Vector3(rayDir.x, rayDir.y, rayDir.z));
}

/* Delete the currently selected point from the path. */
void ofApp::deleteSelectedPathPoint() {
    if (mPathSelectedIndex != -1) {
        // Delete the point from the path
        mPath.deletePointAtIndex(mPathSelectedIndex);
        mPathSelected = false;
        mPathSelectedIndex = -1;
        
        // Hide the point from view
        bTerrainPointSelected = false;
    }
}

void ofApp::promptSaveMotionPath() {
    ofFileDialogResult result = ofSystemSaveDialog("myMotionPath.csv", "Save");
    if(result.bSuccess) {
        cout << "File path successfully retrieved." << endl;
        string filepath = result.getPath();
        ofstream file;
        file.open (filepath, ios::out | ios::trunc);
        
        // Save each point in vertex list to the file using the CSV format
        for (const auto & point : mPath.getPoints()) {
            file << point.x << "," << point.y << "," << point.z << endl;
        }
        
        file.close();
    }
}

void ofApp::promptLoadMotionPath() {
    ofFileDialogResult result = ofSystemLoadDialog("Load Motion Path File");
    if(result.bSuccess) {
        string filepath = result.getPath();
        ifstream file;
        file.open (filepath, ios::in);
        vector<ofVec3f> points;
        
        // Read in file and generate a list of points from CSV file
        if (file.is_open()) {
            string line;
            float x, y, z;
            char chomp;
            
            while (getline(file, line)) {
                istringstream iss(line);
                iss >> x >> chomp >> y >> chomp >> z;
                points.push_back(ofVec3f(x, y, z));
            }
        }
        
        file.close();
        
        // Generate the new path
        mPath.createPathFromList(points);
    }
}

void ofApp::resetCam() {
    cam.reset();
    camMode = 0;
}
void ofApp::chooseCam(int mode) {
    if (mode == 0) {
        resetCam();
    } else {
        camMode = mode;
    }
}
void ofApp::cycleCam() {
    camMode = (camMode + 1) % 5;
    if (camMode == 0) {
        cam.reset();
    }
    // CURRENTLY ONLY SUPPORTS THE FIRST 4 CAMS. 5TH REAR CAM TO BE CONSIDERED...
}
void ofApp::chooseRoverCam() {
    camMode = 1;
}
void ofApp::chooseTrackingCam() {
    camMode = 2;
}
void ofApp::chooseFollowCam() {
    camMode = 3;
}
void ofApp::chooseRearCam() {
    camMode = 4;
}
