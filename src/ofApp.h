#pragma once

#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "ofxGui.h"
#include "box.h"
#include "ray.h"
#include "Octree.hpp"
#include "Model.hpp"
#include "MotionPath.hpp"

class ofApp : public ofBaseApp{

public:
    ~ofApp();
    
    /*****************************************************************************
     * Member methods
     *****************************************************************************/
    // OpenFrameworks Functions
    void setup();
    void update();
    void draw();
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    // Scene manipulation functions
    void initLightingAndMaterials();
    void setCameraTarget();
    void toggleWireframeMode();
    void togglePointsDisplay();
    void toggleSelectTerrain();
    void togglePointSelectionMode();
    void toggleMotionPathSelectionMode();
    void toggleAnimation();
    
    // Cam mode
    void resetCam(); // resets to original mode looking at terrain
    void chooseCam(int);
    void cycleCam();
    void chooseRoverCam();
    void chooseTrackingCam();
    void chooseFollowCam();
    void chooseRearCam();
    
    // Utility functions
    // Cannot be migrated due to reliance on ofApp members to work
    Ray getMouseRay();

    // Load and Save functions
    void savePicture();
    void promptSaveMotionPath();
    void promptLoadMotionPath();

    // Custom modular functions
    bool doPointSelection(const Ray &ray, const bool verbose);

    /*****************************************************************************
     * Member variables
     *****************************************************************************/
    uint64_t currMicrosecs;
    
    // Scene properties
    ofEasyCam cam;
    int camMode; // Modes {0: reset view, 1: rover cam, 2: tracking cam, 3: follow, 4: rear}
    Model mars, rover;
    bool bRoverLoaded;
    
    // Octree
    Octree *octreeMars;
    
    // Motion Path
    bool bMotionPathSelectionMode;
    MotionPath mPath;
    bool mPathSelected;
    int mPathSelectedIndex;
    void deleteSelectedPathPoint();
    
    // Animation
    bool bPlay;
    
    // GUI
    ofxPanel gui;
    ofxFloatSlider speedSlider;
    ofxButton cam0, cam1, cam2, cam3, cam4;
    
    // Keypress states
    bool bAltKeyDown;
    bool bCtrlKeyDown;
    
    // Drawing properties
    int drawingMode; // Modes: {Shaded Face Mode: 0, Wireframe: 1, Vertex/Point: 2}
    
    // Selection properties
    int selectedModel; // Modes: {None: 0, Terrain: 1, Rover: 2}
    bool bPointSelectionMode;
    bool bTerrainPointSelected;
    int selectedVertex;
    ofVec3f selectedPoint;
    ofVec3f intersectPoint;
    
    // Temporary Variables
    ofVec3f tempPosition, tempRoverPosition; // Used to translate rover without glaring jump due to intitial mouseDrag event.
};
